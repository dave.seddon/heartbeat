package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"runtime"
	"sync"
	"time"

	hclog "github.com/hashicorp/go-hclog"
	"github.com/pkg/profile"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/dave.seddon/heartbeat/pkg/hbproto"
	"gitlab.com/dave.seddon/heartbeat/pkg/server"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	MaxPacketSizeCst = 1000
)

var (
	// Passed by "go build -ldflags" for the show version
	tag    string
	commit string
	date   string
)

// // SetupInterruptHandler catches the control c, and closes the doneAll channel
// func SetupInterruptHandler(s *server.ServerT) {
// 	c := make(chan os.Signal)
// 	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
// 	go func() {
// 		<-c
// 		fmt.Println("\r- Ctrl+C pressed in Terminal, calling Shutdown()")
// 		s.Shutdown()
// 		fmt.Println("SetupInterruptHandler complete - bye bye")
// 		os.Exit(0)
// 	}()
// }

func main() {

	port := flag.Int("port", 2000, "UDP port to listen on")
	deadline := flag.Duration("deadline", 10*time.Second, "Read deadline")

	pulse := flag.Duration("pulse", 2*time.Second, "Pulse frequency")
	configF := flag.Duration("configF", 1*time.Hour, "Config check frequency")

	pktSize := flag.Int("pktSize", MaxPacketSizeCst, "Maximum UDP packet receive size")
	sockets := flag.Int("sockets", 4, "Number of so_reuse sockets to open")
	readers := flag.Int("readers", 4, "Number of goroutine readers per socket")

	version := flag.Bool("version", false, "show version")
	logLevel := flag.String("log", "info", "Log level: NoLevel, Trace, Debug, Info, Warn, Error, Off, default \"Info\"")
	grpcBind := flag.String("grpcBind", ":8890", "gRPC bind socket")
	promBind := flag.String("promBind", ":8889", "Prometheus /metrics HTTP bind socket")
	promPath := flag.String("promPath", "/metrics", "Prometheus metrics path \"/metrics\"")
	profMode := flag.String("prof", "", "enable profiling mode, options [cpu, mem, mutex, block, trace]")
	limitGoMaxProcs := flag.Bool("limitGoMaxProcs", false, "limit GoMaxProcs")
	goMaxProcs := flag.Int("goMaxProcs", 4, "goMaxProcs = https://golang.org/pkg/runtime/#GOMAXPROCS")

	dn := flag.Int("dn", 11, "Server.New debug level")
	du := flag.Int("du", 11, "Server.UDPReader debug level")

	flag.Parse()

	if *version {
		fmt.Println("monitor\ttag:", tag, "\tcommit:", commit, "\tcompile date(UTC):", date)
		os.Exit(0)
	}

	logger := hclog.New(&hclog.LoggerOptions{
		Name:  "m",
		Level: hclog.LevelFromString(*logLevel),
	})

	if *limitGoMaxProcs {
		mp := runtime.GOMAXPROCS(*goMaxProcs)
		logger.Info(fmt.Sprintf("Main runtime.GOMAXPROCS was:%d", mp))
	}

	//-----------------------------------------------
	// Profiling

	logger.Info(fmt.Sprintf("*profMode:%s", *profMode))
	switch *profMode {
	case "cpu":
		defer profile.Start(profile.CPUProfile, profile.ProfilePath(".")).Stop()
	case "mem":
		defer profile.Start(profile.MemProfile, profile.ProfilePath(".")).Stop() // heap
	case "mutex":
		defer profile.Start(profile.MutexProfile, profile.ProfilePath(".")).Stop()
	case "block":
		defer profile.Start(profile.BlockProfile, profile.ProfilePath(".")).Stop()
	case "trace":
		defer profile.Start(profile.TraceProfile, profile.ProfilePath(".")).Stop()
	default:
		logger.Info("No profiling")
	}

	//-----------------------------------------------
	// Prometheus Setup
	http.Handle(*promPath, promhttp.HandlerFor(
		prometheus.DefaultGatherer,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	))
	go http.ListenAndServe(*promBind, nil)

	logger.Info("Prometheus http listener started", "*promBind", *promBind, "*promPath", *promPath)

	// Setup complete
	//-------------------------------------------------------------------------

	gs := grpc.NewServer()

	wg := new(sync.WaitGroup)
	wg.Add(1)
	doneAll := make(chan struct{}, 2)

	if *port > 65535 {
		log.Fatal("port > 65535")
	}
	udpPort := uint16(*port)

	debugLevels := server.DebugLevelsT{
		New:       *dn,
		UDPReader: *du,
	}

	srv := server.NewFullConfig(logger, doneAll, wg, *pulse, *configF, udpPort, *deadline, *pktSize, *sockets, *readers, debugLevels)

	//SetupInterruptHandler(srv)

	hbproto.RegisterHeartBeatServer(gs, srv)

	reflection.Register(gs)

	l, err := net.Listen("tcp", *grpcBind)
	if err != nil {
		logger.Error("Unable to create listener", "err", err)
		os.Exit(1)
	} else {
		logger.Info("gRPC listener started with reflection enabled", "*grpcBind", *grpcBind)
	}

	sr := gs.Serve(l)
	if sr != nil {
		logger.Error("Unable gs.Serve(l)", "err", sr)
	}

	//-----------------------------------------------

	logger.Info("wg.Wait")
	wg.Wait()

	logger.Info("Completed.  Bye bye")

}
