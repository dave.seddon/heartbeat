package main

// radix test toy for experimentation
// mostly copied from
// https://github.com/mediocregopher/radix/blob/v4/bench/bench_test.go

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/mediocregopher/radix/v4"
	"github.com/pkg/profile"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/dave.seddon/heartbeat/pkg/hbredis"
	"inet.af/netaddr"
)

// "gitlab.com/dave.seddon/heartbeat/hbproto"
// "gitlab.com/dave.seddon/heartbeat/pkg/hbredis"

var (
	// "go build -ldflags" for the show version
	commit string
	date   string
)

func main() {
	key := flag.String("key", "1.1.1.1", "key to set")
	value := flag.String("value", "2.2.2.2", "value to set")
	seconds := flag.String("seconds", "10", "SETEX ttl in seconds")

	network := flag.String("network", "tcp", "Socket bind type, \"tcp\" or \"unix\"")
	unix := flag.String("unix", "/run/redis/redis.sock", "Unix socket file")
	tcp := flag.String("tcp", "localhost:6379", "TCP socket address")

	version := flag.Bool("version", false, "show version")

	promBind := flag.String("promBind", ":8888", "Prometheus /metrics HTTP bind socket")
	promPath := flag.String("promPath", "/metrics", "Prometheus metrics path")
	profMode := flag.String("prof", "", "enable profiling mode, options [cpu, mem, mutex, block, trace]")
	logLevel := flag.String("log", "info", "Log level: NoLevel, Trace, Debug, Info, Warn, Error, Off")

	flag.Parse()

	if *version {
		fmt.Println(os.Args[0], "\t commit:", commit, "\t compile date:", date)
		os.Exit(0)
	}

	logger := hclog.New(&hclog.LoggerOptions{
		Name:  os.Args[0],
		Level: hclog.LevelFromString(*logLevel),
	})

	//-----------------------------------------------
	// Profiling
	logger.Info(fmt.Sprintf("*profMode:%s", *profMode))
	switch *profMode {
	case "cpu":
		defer profile.Start(profile.CPUProfile, profile.ProfilePath(".")).Stop()
	case "mem":
		defer profile.Start(profile.MemProfile, profile.ProfilePath(".")).Stop() // heap
	case "mutex":
		defer profile.Start(profile.MutexProfile, profile.ProfilePath(".")).Stop()
	case "block":
		defer profile.Start(profile.BlockProfile, profile.ProfilePath(".")).Stop()
	case "trace":
		defer profile.Start(profile.TraceProfile, profile.ProfilePath(".")).Stop()
	default:
		logger.Info("No profiling")
	}

	//-----------------------------------------------
	// Prometheus Setup
	http.Handle(*promPath, promhttp.HandlerFor(
		prometheus.DefaultGatherer,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	))
	go http.ListenAndServe(*promBind, nil)

	logger.Info("Prometheus http listener started", "*promBind", *promBind, "*promPath", *promPath)

	//-----------------------------------------------
	ctx := context.Background()

	//-----------------------------------------------
	addr, err := hbredis.GetAddr(network, tcp, unix)
	if err != nil {
		log.Fatal(err)
	}

	//-----------------------------------------------

	client := hbredis.ConnectRadixV4(ctx, *network, addr)
	defer client.Close()

	logger.Info(client.Addr().String())

	//-----------------------------------------------
	pong := hbredis.PingRadixV4(ctx, client)

	logger.Info(fmt.Sprintf("ping:%s", pong))

	//-----------------------------------------------
	start := time.Now()
	keyByteSlc, valueByteSlc := hbredis.IPStringsAsByteSlice(*key, *value)
	conversion := time.Since(start)

	//-----------------------------------------------
	// SET into Redis
	set, setexDur := hbredis.SetExBytesRadixV4(ctx, client, &keyByteSlc, &valueByteSlc, *seconds)
	logger.Info(fmt.Sprintf("set:%s \t conversion:%s \t setexDur:%s", set, conversion, setexDur))

	//-----------------------------------------------

	var out []byte

	for i := 0; i < 12; i++ {
		client.Do(ctx, radix.FlatCmd(&out, "GET", keyByteSlc))

		var valueReturnedIP netaddr.IP
		vrErr := valueReturnedIP.UnmarshalBinary(out)
		if vrErr != nil {
			log.Fatal(vrErr)
		}
		logger.Info(fmt.Sprintf("i:%d \t valueReturnedIP:%s \t time.Sleep(1 * time.Second)", i, valueReturnedIP.String()))

		time.Sleep(1 * time.Second)
	}

	os.Exit(0)
}
