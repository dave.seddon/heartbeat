package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"

	hclog "github.com/hashicorp/go-hclog"
	"github.com/pkg/profile"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/dave.seddon/heartbeat/pkg/hbproto"
	"google.golang.org/protobuf/types/known/timestamppb"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"inet.af/netaddr"
)

// "gitlab.com/dave.seddon/heartbeat/hbproto"

var (
	// "go build -ldflags" for the show version
	commit string
	date   string
	me     string = "hbc"

	pool = sync.Pool{
		New: func() interface{} {
			return &bytes.Buffer{}
		},
	}

	promI = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: me,
		Subsystem: "loop",
		Name:      "loops",
		Help:      "main loops counter",
	})

	promBytes = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: me,
		Subsystem: "loop",
		Name:      "bytes",
		Help:      "main loop bytes counter",
	})

	promWE = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: me,
		Subsystem: "loop",
		Name:      "write_errs",
		Help:      "UDP write errors",
	})

	promTE = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: me,
		Subsystem: "loop",
		Name:      "write_timeout_errs",
		Help:      "UDP write timeout errors",
	})

	promRE = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: me,
		Subsystem: "loop",
		Name:      "write_refused_errs",
		Help:      "UDP write refused errors (on loopback this means the server isn't listening)",
	})
)

func main() {

	serial := flag.String("serial", "abcdefg123456", "Device serial number")
	dest := flag.String("dest", "127.0.0.1:2000", "Dest for the UDP heartbeats")
	//dest := flag.String("dest", "[::]:2000", "Dest for the UDP heartbeats")
	//freq := flag.Duration("freq", 2*time.Second, "Heartbeat frequency")
	deadline := flag.Duration("deadline", 100*time.Millisecond, "Write deadline")
	version := flag.Bool("version", false, "show version")
	grpcBind := flag.String("grpcBind", ":8890", "gRPC bind socket")
	promBind := flag.String("promBind", ":8888", "Prometheus /metrics HTTP bind socket")
	promPath := flag.String("promPath", "/metrics", "Prometheus metrics path")
	profMode := flag.String("prof", "", "enable profiling mode, options [cpu, mem, mutex, block, trace]")
	logLevel := flag.String("log", "info", "Log level: NoLevel, Trace, Debug, Info, Warn, Error, Off")
	limitGoMaxProcs := flag.Bool("limitGoMaxProcs", true, "limit GoMaxProcs")
	goMaxProcs := flag.Int("goMaxProcs", 4, "goMaxProcs = https://golang.org/pkg/runtime/#GOMAXPROCS")
	flag.Parse()

	if *version {
		fmt.Println(me, "\t commit:", commit, "\t compile date:", date)
		os.Exit(0)
	}

	logger := hclog.New(&hclog.LoggerOptions{
		Name:  me,
		Level: hclog.LevelFromString(*logLevel),
	})

	if *limitGoMaxProcs {
		mp := runtime.GOMAXPROCS(*goMaxProcs)
		logger.Info(fmt.Sprintf("Main runtime.GOMAXPROCS was:%d", mp))
	}

	//-----------------------------------------------
	// Profiling
	logger.Info(fmt.Sprintf("*profMode:%s", *profMode))
	switch *profMode {
	case "cpu":
		defer profile.Start(profile.CPUProfile, profile.ProfilePath(".")).Stop()
	case "mem":
		defer profile.Start(profile.MemProfile, profile.ProfilePath(".")).Stop() // heap
	case "mutex":
		defer profile.Start(profile.MutexProfile, profile.ProfilePath(".")).Stop()
	case "block":
		defer profile.Start(profile.BlockProfile, profile.ProfilePath(".")).Stop()
	case "trace":
		defer profile.Start(profile.TraceProfile, profile.ProfilePath(".")).Stop()
	default:
		logger.Info("No profiling")
	}

	//-----------------------------------------------
	// Prometheus Setup
	http.Handle(*promPath, promhttp.HandlerFor(
		prometheus.DefaultGatherer,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	))
	go http.ListenAndServe(*promBind, nil)

	logger.Info("Prometheus http listener started", "*promBind", *promBind, "*promPath", *promPath)

	//------------------------------------------------
	conn, err := grpc.Dial(*grpcBind, grpc.WithInsecure())
	if err != nil {
		log.Fatal("grpc.Dial(*grpcBind, grpc.WithInsecure() error:", err)
	}
	client := hbproto.NewHeartBeatClient(conn)

	e := &hbproto.EmptyT{}
	clientConfigPB, err := client.GetClientConfig(context.Background(), e)
	if err != nil {
		log.Fatal("client.GetClientConfig(context.Background(), e):", err)
	}
	// publicKeyPB, err := client.GetPublicKey(context.Background(), e)
	// if err != nil {
	// 	log.Fatal("client.GetPublicKey(context.Background(), e):", err)
	// }

	publicKey, err := x509.ParsePKCS1PublicKey(clientConfigPB.PublicKey.GetPublicKey())
	if err != nil {
		log.Fatal("x509.ParsePKCS1PublicKey(publicKeyPB.GetPublicKey()):", err)
	}

	// cphr, err := aes.NewCipher(publicKey)
	// if err != nil {
	// 	log.Fatal("aes.NewCipher(publicKey):", err)
	// }
	// gcm, err := cipher.NewGCM(cphr)
	// if err != nil {
	// 	log.Fatal("cipher.NewGCM(cphr):", err)
	// }

	//-------------------------------------------------
	// Hostname
	hostname, hErr := os.Hostname()
	if hErr != nil {
		log.Fatal(hErr)
	}

	//-------------------------------------------------
	// Open UDP socket
	udpSock, dialErr := net.Dial("udp", *dest)
	if dialErr != nil {
		log.Fatal(dialErr)
	}
	defer udpSock.Close()
	logger.Info(fmt.Sprintf("Open socket:%s", udpSock.LocalAddr().String()))

	// https://pkg.go.dev/inet.af/netaddr#FromStdIP
	localIP, ok := netaddr.FromStdIP(udpSock.RemoteAddr().(*net.UDPAddr).IP)
	if !ok {
		log.Fatal("netaddr.FromStdIP(udpSock.RemoteAddr().(*net.UDPAddr).IP) error")
	}
	logger.Info(localIP.String())
	localIPBytes, err := localIP.MarshalBinary()
	if err != nil {
		log.Fatal(err)
	}

	ticker := time.NewTicker(clientConfigPB.PulseFrequency.Duration.AsDuration())
	configTicker := time.NewTicker(clientConfigPB.ConfigFrequency.Duration.AsDuration())
	var loops uint64
	var bytesWritten int
	var writeErrs int
	var writeTimeouts int
	var writeRefused int
	var dlErrs int
	for ; ; loops++ {

		buf := pool.Get().(*bytes.Buffer)

		heartbeatProto := &hbproto.HeartBeatT{
			Timestamp: timestamppb.Now(),
			Sequence:  wrapperspb.UInt64(loops),
			Hostname:  wrapperspb.String(hostname),
			Serial:    wrapperspb.String(*serial),
			LANIP:     wrapperspb.Bytes(localIPBytes),
		}

		logger.Info(heartbeatProto.String())

		// https://pkg.go.dev/google.golang.org/protobuf/proto?tab=doc#Marshal
		protoWire, marshalErr := proto.Marshal(heartbeatProto)
		if marshalErr != nil {
			fmt.Println("proto.Marshal(heartbeatProto) error: ", marshalErr)
		}

		ciphertext, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, protoWire, nil)
		if err != nil {
			log.Fatal("rsa.EncryptOAEP(sha256.New(), rand.Reader, publicKey, protoWire, nil):", err)
		}
		buf.Write(ciphertext)
		//buf.Write(protoWire)

		dlErr := udpSock.SetWriteDeadline(time.Now().Add(*deadline))
		if dlErr != nil {
			dlErrs++
			return
		}

		n, wErr := udpSock.Write(buf.Bytes())
		if wErr != nil {
			if wErr.(net.Error).Timeout() {
				writeTimeouts++
				promTE.Inc()
			} else {
				if strings.Contains(wErr.Error(), "connection refused") {
					writeRefused++
					promRE.Inc()
				} else {
					writeErrs++
					promWE.Inc()
				}
			}
			logger.Info(fmt.Sprintf("%s", wErr))
		}

		promI.Inc()
		bytesWritten += n
		promBytes.Add(float64(n))

		buf.Reset()
		pool.Put(buf)

		select {
		case <-configTicker.C:
			clientConfigPB, err = client.GetClientConfig(context.Background(), e)
			if err != nil {
				log.Fatal("client.GetClientConfig(context.Background(), e):", err)
			}
			ticker = time.NewTicker(clientConfigPB.PulseFrequency.Duration.AsDuration())
			configTicker = time.NewTicker(clientConfigPB.ConfigFrequency.Duration.AsDuration())
			publicKey, err = x509.ParsePKCS1PublicKey(clientConfigPB.PublicKey.GetPublicKey())
			if err != nil {
				log.Fatal("x509.ParsePKCS1PublicKey(publicKeyPB.GetPublicKey()):", err)
			}
		default:
			// non-blocking
		}

		<-ticker.C

		logger.Info(fmt.Sprintf("%s \t loops:%d \t n:%d \t bytesWritten:%d \t wErrs:%d \t wTOuts:%d \t wRefused:%d", *dest, loops, n, bytesWritten, writeErrs, writeTimeouts, writeRefused))
	}
}
