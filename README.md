# heartbeat

# gRPC

An excellent tool for talking to gRPC is

https://github.com/fullstorydev/grpcurl

Install grpcurl via:

```
go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest
```

This works because the reflection is enabled for the grpc service via:
```
reflection.Register(gs)
```

Example of using listing services
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/server$ grpcurl --plaintext localhost:8890 list
grpc.reflection.v1alpha.ServerReflection
hbproto.HeartBeat
```

List the HeartBeat gRPCs
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/hbs$ grpcurl --plaintext localhost:8890 list hbproto.HeartBeat
hbproto.HeartBeat.GetPublicKey
hbproto.HeartBeat.Pulse
```

Describe the HeartBeat Pulse gRPCs
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/hbs$ grpcurl --plaintext localhost:8890 describe hbproto.HeartBeat.Pulse
hbproto.HeartBeat.Pulse is a method:
rpc Pulse ( .hbproto.HeartBeatT ) returns ( .hbproto.EmptyT );
```

Describe the HeartBeatT protobuf
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/hbs$ grpcurl --plaintext localhost:8890 describe .hbproto.HeartBeatT
hbproto.HeartBeatT is a message:
message HeartBeatT {
  .google.protobuf.Timestamp Timestamp = 1;
  .google.protobuf.UInt64Value Sequence = 10;
  .google.protobuf.StringValue Hostname = 20;
  .google.protobuf.StringValue Serial = 21;
  .google.protobuf.StringValue Ssid = 22;
  .google.protobuf.BytesValue RouterIP = 30;
  .google.protobuf.BytesValue LANIP = 31;
}
```

hbproto.EmptyT
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/pkg/hbproto$ grpcurl --plaintext localhost:8890 describe .hbproto.EmptyT
hbproto.EmptyT is a message:
message EmptyT {
}
```

GetPublicKey
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/pkg/hbproto$ grpcurl --plaintext -d '{ }' localhost:8890 hbproto.HeartBeat.GetPublicKey
{
  "PublicKey": "3zLoDhN0sMe8KZrkadwQthTWz8V8DgmoX1leq58sAM8="
}

```


# gRPC
WARNING.  Please note ./pkg/hbproto/Makefile protoc call IS using "require_unimplemented_servers=false".

TODO (seddon) Read up on this and correct this.

https://github.com/grpc/grpc-go/releases/tag/cmd%2Fprotoc-gen-go-grpc%2Fv1.0.0

# Prometheus stats
The UDPPromStats function sets up Prometheus https://pkg.go.dev/github.com/prometheus/client_golang/prometheus/promauto NewCounterVec-ers, where the labeled counters are incremented within each UDPReader worker.

```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/hbc$ curl -s http://127.0.0.1:8889/metrics 2>&1 | grep -v "#" | grep UDP
UDPReader_ReadFrom_Duration{i="1",j="0",quantile="0.5"} 0.039788825
UDPReader_ReadFrom_Duration{i="1",j="0",quantile="0.99"} 0.099691504
UDPReader_ReadFrom_Duration_sum{i="1",j="0"} 24.113742026000008
UDPReader_ReadFrom_Duration_count{i="1",j="0"} 616
UDPReader_ReadFrom_Duration{i="1",j="1",quantile="0.5"} 0.039798005
UDPReader_ReadFrom_Duration{i="1",j="1",quantile="0.99"} 0.099923022
UDPReader_ReadFrom_Duration_sum{i="1",j="1"} 24.10877660899998
UDPReader_ReadFrom_Duration_count{i="1",j="1"} 614
UDPReader_ReadFrom_Duration{i="1",j="2",quantile="0.5"} 0.039800595
UDPReader_ReadFrom_Duration{i="1",j="2",quantile="0.99"} 0.100599141
UDPReader_ReadFrom_Duration_sum{i="1",j="2"} 24.10063692800004
UDPReader_ReadFrom_Duration_count{i="1",j="2"} 593
UDPReader_ReadFrom_Duration{i="1",j="3",quantile="0.5"} 0.039802901
UDPReader_ReadFrom_Duration{i="1",j="3",quantile="0.99"} 0.100204297
UDPReader_ReadFrom_Duration_sum{i="1",j="3"} 24.089323735
UDPReader_ReadFrom_Duration_count{i="1",j="3"} 599
UDPReader_insert_grpcDuration{i="1",j="0",quantile="0.5"} 0.000135251
UDPReader_insert_grpcDuration{i="1",j="0",quantile="0.99"} 0.000226954
UDPReader_insert_grpcDuration_sum{i="1",j="0"} 0.08145087200000002
UDPReader_insert_grpcDuration_count{i="1",j="0"} 616
UDPReader_insert_grpcDuration{i="1",j="1",quantile="0.5"} 0.000135148
UDPReader_insert_grpcDuration{i="1",j="1",quantile="0.99"} 0.000215254
UDPReader_insert_grpcDuration_sum{i="1",j="1"} 0.0804229379999999
UDPReader_insert_grpcDuration_count{i="1",j="1"} 614
UDPReader_insert_grpcDuration{i="1",j="2",quantile="0.5"} 0.000134851
UDPReader_insert_grpcDuration{i="1",j="2",quantile="0.99"} 0.000222223
UDPReader_insert_grpcDuration_sum{i="1",j="2"} 0.07826159400000002
UDPReader_insert_grpcDuration_count{i="1",j="2"} 593
UDPReader_insert_grpcDuration{i="1",j="3",quantile="0.5"} 0.000135513
UDPReader_insert_grpcDuration{i="1",j="3",quantile="0.99"} 0.000283539
UDPReader_insert_grpcDuration_sum{i="1",j="3"} 0.08048023499999997
UDPReader_insert_grpcDuration_count{i="1",j="3"} 599
hbs_UDPReader_bytesRead{i="1",j="0"} 36800
hbs_UDPReader_bytesRead{i="1",j="1"} 36679
hbs_UDPReader_bytesRead{i="1",j="2"} 35408
hbs_UDPReader_bytesRead{i="1",j="3"} 35785
hbs_UDPReader_loops{i="0",j="0"} 2
hbs_UDPReader_loops{i="1",j="0"} 616
hbs_UDPReader_loops{i="1",j="1"} 614
hbs_UDPReader_loops{i="1",j="2"} 593
hbs_UDPReader_loops{i="1",j="3"} 599
hbs_UDPReader_loops{i="2",j="0"} 2
hbs_UDPReader_loops{i="3",j="2"} 2
hbs_UDPReader_readTimeouts{i="0",j="0"} 2
hbs_UDPReader_readTimeouts{i="2",j="0"} 2
hbs_UDPReader_readTimeouts{i="3",j="2"} 2
```