module gitlab.com/dave.seddon/heartbeat

go 1.17

//replace gitlab.com/dave.seddon/heartbeat/ => ./

require (
	github.com/go-redis/redis/v7 v7.4.1
	github.com/hashicorp/go-hclog v0.16.2
	github.com/mediocregopher/radix/v4 v4.0.0-beta.1
	github.com/pkg/profile v1.6.0
	github.com/prometheus/client_golang v1.11.0
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
	inet.af/netaddr v0.0.0-20210729200904-31d5ee66059c
)

require (
	bou.ke/monkey v1.0.2 // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/fatih/color v1.12.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/prometheus/common v0.30.0 // indirect
	github.com/prometheus/procfs v0.7.3 // indirect
	github.com/tilinna/clock v1.0.2 // indirect
	go4.org/intern v0.0.0-20210108033219-3eb7198706b2 // indirect
	go4.org/unsafe/assume-no-moving-gc v0.0.0-20201222180813-1025295fd063 // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20210830153122-0bac4d21c8ea // indirect
)
