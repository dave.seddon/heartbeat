COMMIT=$(shell /bin/git describe --always)
DATE=$(shell /bin/date -u +"%Y-%m-%d-%H:%M")

all: clean build

build:
	# https://blog.filippo.io/shrink-your-go-binaries-with-this-one-weird-trick/
	go build -ldflags "-s -w -X main.commit=${COMMIT} -X main.date=${DATE}" -o ./cmd/hbc/hbc ./cmd/hbc/main.go
	ls -lah ./cmd/hbc/
	echo "hbc*" > "./cmd/hbc/.gitignore"
	./cmd/hbc/hbc --version

	go build -ldflags "-s -w -X main.commit=${COMMIT} -X main.date=${DATE}" -o ./cmd/hbs/hbs ./cmd/hbs/main.go
	ls -lah ./cmd/hbs/
	echo "hbs*" > "./cmd/hbs/.gitignore"
	./cmd/hbs/hbs --version

clean:
	rm -rf ./cmd/hbc/hbc*
	rm -rf ./cmd/hbs/hbs*

update:
	go mod tidy
	go mod verify
	go get -u ./...
	go mod tidy
	go mod verify
