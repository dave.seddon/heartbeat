# Redis install on ubuntu instructions

These are basic instructions for installing redis server on ubuntu for local development.


```
sudo add-apt-repository ppa:redislabs/redis
sudo apt-get update
sudo apt-get install redis
```

I had problems with the service being "masked", which was corrected by:
```
sudo systemctl daemon-reload;
sudo systemctl unmask redis-server.service
sudo systemctl start redis-server.service
sudo systemctl enable --now redis-server.service
sudo systemctl daemon-reload;
```

Default config lines to change
```
bind 127.0.0.1 -::1
# unixsocket /run/redis.sock
# unixsocketperm 700
timeout 0
tcp-keepalive 300
databases 16
proc-title-template "{title} {listen-addr} {server-mode}"
```

Update the config - WARNING see unix socket permisssions!!!  TESTING ONLY!!!
```
sudo sed -i /etc/redis/redis.conf -e 's/^bind 127.0.0.1 -::1/bind 127.0.0.1 ::1/'
sudo sed -i /etc/redis/redis.conf -e 's/^# unixsocket \/run\/redis.sock/unixsocket \/run\/redis\/redis.sock/'
sudo sed -i /etc/redis/redis.conf -e 's/^# unixsocketperm 700/unixsocketperm 777/'
sudo sed -i /etc/redis/redis.conf -e 's/^timeout 0/timeout 60/'
sudo sed -i /etc/redis/redis.conf -e 's/^tcp-keepalive 300/tcp-keepalive 10/'
sudo sed -i /etc/redis/redis.conf -e 's/^databases 16/databases 4/'
sudo sed -i /etc/redis/redis.conf -e 's/^proc-title-template "{title} {listen-addr} {server-mode}"/proc-title-template "{title} {listen-addr} {unixsocket} {config-file} {server-mode}/'
```



Diff
```
root@das-tpi9:/home/das/Downloads# diff -u /etc/redis/original.redis.conf /etc/redis/redis.conf
--- /etc/redis/original.redis.conf	2021-08-31 16:20:52.541742891 -0700
+++ /etc/redis/redis.conf	2021-08-31 17:50:18.723547138 -0700
@@ -72,7 +72,8 @@
 # IF YOU ARE SURE YOU WANT YOUR INSTANCE TO LISTEN TO ALL THE INTERFACES
 # JUST COMMENT OUT THE FOLLOWING LINE.
 # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-bind 127.0.0.1 -::1
+#bind 127.0.0.1 -::1
+bind 127.0.0.1 ::1

 # Protected mode is a layer of security protection, in order to avoid that
 # Redis instances left open on the internet are accessed and exploited.
@@ -112,11 +113,11 @@
 # incoming connections. There is no default, so Redis will not listen
 # on a unix socket when not specified.
 #
-# unixsocket /run/redis.sock
-# unixsocketperm 700
+unixsocket /run/redis/redis.sock
+unixsocketperm 777

 # Close the connection after a client is idle for N seconds (0 to disable)
-timeout 0
+timeout 60

 # TCP keepalive.
 #
@@ -133,7 +134,7 @@
 #
 # A reasonable value for this option is 300 seconds, which is the new
 # Redis default starting with Redis 3.2.1.
-tcp-keepalive 300
+tcp-keepalive 10

 ################################# TLS/SSL #####################################

@@ -272,7 +273,8 @@
 # The default is "no". To run under upstart/systemd, you can simply uncomment
 # the line below:
 #
-supervised auto
+#supervised auto
+supervised systemd

 # If a pid file is specified, Redis writes it where specified at startup
 # and removes it at exit.
@@ -324,7 +326,7 @@
 # Set the number of databases. The default database is DB 0, you can select
 # a different one on a per-connection basis using SELECT <dbid> where
 # dbid is a number between 0 and 'databases'-1
-databases 16
+databases 4

 # By default Redis shows an ASCII art logo only when started to log to the
 # standard output and if the standard output is a TTY and syslog logging is
@@ -355,7 +357,8 @@
 # {unixsocket}      Unix domain socket listening on, or "".
 # {config-file}     Name of configuration file used.
 #
-proc-title-template "{title} {listen-addr} {server-mode}"
+#proc-title-template "{title} {listen-addr} {server-mode}"
+proc-title-template "{title} {listen-addr} {unixsocket} {config-file} {server-mode}"

 ################################ SNAPSHOTTING  ################################

```

https://redis.io/topics/rediscli


https://redis.io/topics/latency
Latency testing...

```
root@das-tpi9:/home/das/Downloads# redis-cli -s /run/redis/redis.sock --latency-history
min: 0, max: 1, avg: 0.18 (1455 samples) -- 15.00 seconds range
min: 0, max: 1, avg: 0.21 (1452 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.20 (1453 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.20 (101 samples)^C
root@das-tpi9:/home/das/Downloads# redis-cli --latency-history
min: 0, max: 1, avg: 0.25 (1447 samples) -- 15.00 seconds range
min: 0, max: 1, avg: 0.27 (1441 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.26 (1441 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.30 (79 samples)^C
root@das-tpi9:/home/das/Downloads# redis-cli --latency-history -h ::1
min: 0, max: 1, avg: 0.30 (1434 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.27 (1439 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.29 (1435 samples) -- 15.01 seconds range
min: 0, max: 1, avg: 0.24 (128 samples)^C
```


TODO
For running on SPA we need to review:
- ACLS
- Memory config (limits/policy)
- Lazy freeing
- io-threads
- oom stuff
- hz defaults to 10



https://redis.uptrace.dev/guide/tracing.html



# OLD INSTRUCTIONS BELOW THAT DON'T WORK - very old Redis

```
sudo apt update
sudo apt upgrade --yes
```

Install redis
```
sudo apt install redis-server
```

Update the config for systemd
```
sudo cp /etc/redis/redis.conf /etc/redis/original.redis.conf
sudo sed -i /etc/redis/redis.conf -e 's/^supervised no/supervised systemd/'
```

Verify
```
das@das-tpi9:~$ sudo diff -u /etc/redis/original.redis.conf /etc/redis/redis.conf
--- /etc/redis/original.redis.conf	2021-08-31 14:40:43.696516453 -0700
+++ /etc/redis/redis.conf	2021-08-31 14:41:44.736869312 -0700-
@@ -144,7 +144,7 @@
 #                        UPSTART_JOB or NOTIFY_SOCKET environment variables
 # Note: these supervision methods only signal "process is ready."
 #       They do not enable continuous liveness pings back to your supervisor.
-supervised no
+supervised systemd

 # If a pid file is specified, Redis writes it where specified at startup
 # and removes it at exit.
```

Restart redis
```
sudo systemctl restart redis.service
```

Verify
```
das@das-tpi9:~$ sudo systemctl restart redis.service
das@das-tpi9:~$ sudo systemctl status redis.service
● redis-server.service - Advanced key-value store
     Loaded: loaded (/lib/systemd/system/redis-server.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-08-31 14:42:38 PDT; 19s ago
       Docs: http://redis.io/documentation,
             man:redis-server(1)
    Process: 8412 ExecStart=/usr/bin/redis-server /etc/redis/redis.conf (code=exited, status=0/SUCCESS)
   Main PID: 8413 (redis-server)
      Tasks: 4 (limit: 37914)
     Memory: 1.9M
     CGroup: /system.slice/redis-server.service
             └─8413 /usr/bin/redis-server 127.0.0.1:6379

Aug 31 14:42:38 das-tpi9 systemd[1]: Starting Advanced key-value store...
Aug 31 14:42:38 das-tpi9 systemd[1]: redis-server.service: Can't open PID file /run/redis/redis-server.pid (yet?) after start: Operatio>
Aug 31 14:42:38 das-tpi9 systemd[1]: Started Advanced key-value store.
```

Limits
```
das@das-tpi9:~$ /bin/cat /proc/"$(systemctl show -p MainPID redis.service | /bin/sed -e 's/MainPID=//')"/limits
Limit                     Soft Limit           Hard Limit           Units
Max cpu time              unlimited            unlimited            seconds
Max file size             unlimited            unlimited            bytes
Max data size             unlimited            unlimited            bytes
Max stack size            8388608              unlimited            bytes
Max core file size        0                    unlimited            bytes
Max resident set          unlimited            unlimited            bytes
Max processes             126380               126380               processes
Max open files            65535                65535                files
Max locked memory         65536                65536                bytes
Max address space         unlimited            unlimited            bytes
Max file locks            unlimited            unlimited            locks
Max pending signals       126380               126380               signals
Max msgqueue size         819200               819200               bytes
Max nice priority         0                    0
Max realtime priority     0                    0
Max realtime timeout      unlimited            unlimited            us
```

Oh no!  Redis version is prehistoric
```
das@das-tpi9:~/go/src/gitlab.com/dave.seddon/heartbeat/cmd/radix_test$ dpkg --list | grep redis
ii  libhiredis0.14:amd64                       0.14.0-6                              amd64        minimalistic C client library for Redis
ii  redis-server                               5:5.0.7-2                             amd64        Persistent key-value database with network interface
ii  redis-tools                                5:5.0.7-2                             amd64        Persistent key-value database with network interface (client)
```

Unix socket??
https://github.com/mediocregopher/radix/issues/231
conn, err = radix.Dial("unix", socket)

conn, err = net.Dial("unix", "/run/redis/redis.sock")
if err != nil {
    // if this is a temporary error, then retry
    if terr, ok := err.(TemporaryError); ok && terr.Temporary() {
        return err
    }
}


https://blog.cryptic.io/brian-picciano-v4-of-radix-a-golang-redis-driver/

https://pkg.go.dev/github.com/mediocregopher/radix/v4#section-documentation

https://pkg.go.dev/github.com/mediocregopher/radix/v4#example-FlatCmd-Struct

https://github.com/antirez/RESP3/blob/master/spec.md

issue with hset expires:
https://github.com/redis/redis/issues/1042
https://github.com/redis/redis/issues/167

https://github.com/Netflix/dyno/wiki/Expire-Hash
https://github.com/alibaba/TairHash

// https://pkg.go.dev/math/big#Int.SetBytes
func ipv6ToInt(IPv6Addr net.IP) *big.Int {
    IPv6Int := big.NewInt(0)
    IPv6Int.SetBytes(IPv6Addr)
    return IPv6Int
}

func ip2int(ip net.IP) uint32 {
	if len(ip) == 16 {
		return binary.BigEndian.Uint32(ip[12:16])
	}
	return binary.BigEndian.Uint32(ip)
}

func int2ip(nn uint32) net.IP {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, nn)
	return ip
}

// https://pkg.go.dev/math/big#Int.SetBytes
func ipv6ToInt(IPv6Addr net.IP) *big.Int {
    IPv6Int := big.NewInt(0)
    IPv6Int.SetBytes(IPv6Addr)
    return IPv6Int
}

func IntToIpv6(intipv6 *big.Int) net.IP {
    ip := intipv6.Bytes()
    var a net.IP = ip
    return a
}