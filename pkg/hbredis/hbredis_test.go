package hbredis

// Benchmarks for RadixV4 verse Go-Redis v7
// Sadly, the client types are different, which makes the code a bit Repetitive
// (time to use generics?)

import (
	"context"
	"fmt"
	"log"
	"math"
	"sync"
	"testing"

	"github.com/go-redis/redis/v7"
	hclog "github.com/hashicorp/go-hclog"
	"github.com/mediocregopher/radix/v4"
	"inet.af/netaddr"
)

const (
	testDebugLevel        int     = 11
	pingK                 float64 = 1
	setExK                float64 = 3
	setExInsertsPerWorker int     = 10
)

// checkTestTable helper function to check the test tables have correct test.i indexing
// Please note - Why do I have test.i?  Just to make it easier to find the test config if there is an error
func checkTestTable(i int, ti int) {
	if i != ti {
		log.Fatal("i is incorrect in the tests table")
	}
}

type GetAddrTestT struct {
	i        int
	network  string
	tcp      string
	unix     string
	addr     string
	expected bool
}

func TestGetAddr(t *testing.T) {
	var tests = []GetAddrTestT{
		{0, "tcp", "localhost:6379", "/run/redis/redis.sock", "localhost:6379", true},
		{1, "unix", "localhost:6379", "/run/redis/redis.sock", "/run/redis/redis.sock", true},
		{2, "fail", "localhost:6379", "/run/redis/redis.sock", "localhost:6379", false},
		{3, "unix", "localhost:6379", "/run/redis/redis.sock", "localhost:6379", false},
		{4, "tcp", "localhost:6379", "/run/redis/redis.sock", "/run/redis/redis.sock", false},
	}
	for i, test := range tests {
		checkTestTable(i, test.i)

		addr, err := GetAddr(&test.network, &test.tcp, &test.unix)
		if err != nil {
			if test.expected {
				log.Fatal(err)
			}
		}
		if addr != test.addr {
			if test.expected {
				t.Errorf(fmt.Sprintf("TestGetAddr \t i:%d \t test.network:%s \t test.tcp:%s \t test.unix:%s \t test.addr:%s \t != addr:%s", i, test.network, test.tcp, test.unix, test.addr, addr))
			}
		}
	}
}

type IPStringsAsByteSliceTestT struct {
	i     int
	key   string
	value string
}

func TestIPStringsAsByteSlice(t *testing.T) {
	var tests = []IPStringsAsByteSliceTestT{
		{0, "1.1.1.1", "2.2.2.2"},
		{1, "2.2.2.2", "1.1.1.1"},
		{2, "ff02::1", "ff02::2"},
		{3, "ff02::2", "ff02::1"},
	}
	for i, test := range tests {
		checkTestTable(i, test.i)

		keyByteSlc, valueByteSlc := IPStringsAsByteSlice(test.key, test.value)

		var kIP netaddr.IP
		var vIP netaddr.IP
		kErr := kIP.UnmarshalBinary(keyByteSlc)
		if kErr != nil {
			log.Fatal(kErr)
		}
		vErr := vIP.UnmarshalBinary(valueByteSlc)
		if vErr != nil {
			log.Fatal(vErr)
		}

		if kIP.String() != test.key {
			t.Errorf(fmt.Sprintf("TestIPStringsAsByteSlice \t i:%d \t test.key:%s \t kIP.String():%s", i, test.key, kIP.String()))
		}
		if vIP.String() != test.value {
			t.Errorf(fmt.Sprintf("TestIPStringsAsByteSlice \t i:%d \t test.value:%s \t vIP.String():%s", i, test.value, vIP.String()))
		}
	}
}

type bmarkConnectT struct {
	i        int
	ctx      context.Context
	network  string
	addr     string
	expected bool
}

func runNameConnect(bmark bmarkConnectT) (runName string) {
	runName = fmt.Sprintf("%d/%s", bmark.i, bmark.network)
	return runName
}

func BenchmarkConnectRadixV4(b *testing.B) {

	bmarks := []bmarkConnectT{
		{0, context.Background(), "tcp", "localhost:6379", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", true},
	}

	logger := hclog.Default()

	for i, bmark := range bmarks {
		b.Run(runNameConnect(bmark), func(b *testing.B) {
			checkTestTable(i, bmark.i)
			for i := 0; i < b.N; i++ {

				client := ConnectRadixV4(bmark.ctx, bmark.network, bmark.addr)
				if testDebugLevel > 100 {
					logger.Info(client.Addr().String())
				}
				client.Close()

			}

		})
	}
}

func BenchmarkConnectGoRedisV7(b *testing.B) {

	bmarks := []bmarkConnectT{
		{0, context.Background(), "tcp", "localhost:6379", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", true},
	}

	logger := hclog.Default()

	for i, bmark := range bmarks {
		b.Run(runNameConnect(bmark), func(b *testing.B) {
			checkTestTable(i, bmark.i)
			for i := 0; i < b.N; i++ {
				client := ConnectGoRedisV7(bmark.network, bmark.addr)
				if testDebugLevel > 100 {
					logger.Info(client.Options().Addr)
				}
				client.Close()

			}

		})
	}
}

func runNamePing(bmark bmarkConnectT, n int) (runName string) {
	//runName = fmt.Sprintf("%d/%s/%s/%d", bmark.i, bmark.network, bmark.addr, n)
	runName = fmt.Sprintf("%d/%s/%d", bmark.i, bmark.network, n)
	return runName
}

func BenchmarkPingRadixV4(b *testing.B) {

	bmarks := []bmarkConnectT{
		{0, context.Background(), "tcp", "localhost:6379", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", true},
	}

	logger := hclog.Default()

	for i, bmark := range bmarks {

		for k := 0.; k <= pingK; k++ {

			n := int(math.Pow(2, k))

			b.Run(runNamePing(bmark, n), func(b *testing.B) {
				checkTestTable(i, bmark.i)

				client := ConnectRadixV4(bmark.ctx, bmark.network, bmark.addr)
				defer client.Close()

				b.ResetTimer()

				for i := 0; i < b.N; i++ {

					for j := 0; j < n; j++ {
						pong := PingRadixV4(bmark.ctx, client)
						if testDebugLevel > 100 {
							logger.Info(fmt.Sprintf("ping:%s", pong))
						}
					}

				}

			})
		}
	}
}

func BenchmarkPingGoRedisV7(b *testing.B) {

	bmarks := []bmarkConnectT{
		{0, context.Background(), "tcp", "localhost:6379", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", true},
	}

	logger := hclog.Default()

	for i, bmark := range bmarks {

		for k := 0.; k <= pingK; k++ {

			n := int(math.Pow(2, k))

			b.Run(runNamePing(bmark, n), func(b *testing.B) {
				checkTestTable(i, bmark.i)

				client := ConnectGoRedisV7(bmark.network, bmark.addr)
				defer client.Close()

				b.ResetTimer()

				for i := 0; i < b.N; i++ {

					for j := 0; j < n; j++ {
						pong := PingGoRedixV7(client)
						if testDebugLevel > 100 {
							logger.Info(fmt.Sprintf("ping:%s", pong))
						}
					}

				}

			})
		}
	}
}

type bmarkSetExT struct {
	i        int
	ctx      context.Context
	network  string
	addr     string
	key      string
	value    string
	seconds  string
	expected bool
}

func runNameSetEx(bmark bmarkSetExT, n int) (runName string) {
	//runName = fmt.Sprintf("%d/%s/%s/%d", bmark.i, bmark.network, bmark.addr, n)
	runName = fmt.Sprintf("%d/%s/inserts%d/%d", bmark.i, bmark.network, setExInsertsPerWorker, n)
	return runName
}

// TODO - Add LOTS more IPs and goroutines!!!
func BenchmarkSetExRadixV4(b *testing.B) {

	bmarks := []bmarkSetExT{
		{0, context.Background(), "tcp", "localhost:6379", "1.1.1.1", "2.2.2.2", "10", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", "1.1.1.1", "2.2.2.2", "10", true},
	}

	logger := hclog.Default()
	var client radix.Client
	var wg sync.WaitGroup

	for i, bmark := range bmarks {

		for k := 0.; k <= setExK; k++ {

			n := int(math.Pow(2, k))

			b.Run(runNameSetEx(bmark, n), func(b *testing.B) {
				checkTestTable(i, bmark.i)

				client = ConnectRadixV4(bmark.ctx, bmark.network, bmark.addr)
				defer client.Close()

				// heartbeat service will receive the key/value as []byte in the protobuf
				// so does not need to be converted, so that's why we're benchmarking
				// with the string->[]byte conversion already completed
				keyByteSlc, valueByteSlc := IPStringsAsByteSlice(bmark.key, bmark.value)

				b.ResetTimer()

				for i := 0; i < b.N; i++ {

					for j := 0; j < n; j++ {
						wg.Add(1)
						go workSetExRadixV4(&wg, logger, client, bmark, &keyByteSlc, &valueByteSlc)
					}
					wg.Wait()
				}

			})
		}
	}
}

func workSetExRadixV4(wg *sync.WaitGroup, logger hclog.Logger, client radix.Client, bmark bmarkSetExT, keyByteSlc *[]byte, valueByteSlc *[]byte) {

	defer wg.Done()

	for i := 0; i < setExInsertsPerWorker; i++ {
		set, setexDur := SetExBytesRadixV4(bmark.ctx, client, keyByteSlc, valueByteSlc, bmark.seconds)
		if testDebugLevel > 100 {
			logger.Info(fmt.Sprintf("set:%s \t setexDur:%s", set, setexDur))
		}
	}
}

func BenchmarkSetExGoRedisV7(b *testing.B) {

	bmarks := []bmarkSetExT{
		{0, context.Background(), "tcp", "localhost:6379", "1.1.1.1", "2.2.2.2", "10", true},
		{1, context.Background(), "unix", "/run/redis/redis.sock", "1.1.1.1", "2.2.2.2", "10", true},
	}

	logger := hclog.Default()
	var client *redis.Client
	var wg sync.WaitGroup

	for i, bmark := range bmarks {

		for k := 0.; k <= setExK; k++ {

			n := int(math.Pow(2, k))

			b.Run(runNameSetEx(bmark, n), func(b *testing.B) {
				checkTestTable(i, bmark.i)

				client = ConnectGoRedisV7(bmark.network, bmark.addr)

				// TODO convert string to []byte, so we CAN include the conversion time in the benchmark

				b.ResetTimer()

				for i := 0; i < b.N; i++ {

					for j := 0; j < n; j++ {
						wg.Add(1)
						go workSetExGoRedisV7(&wg, logger, client, bmark)
					}
					wg.Wait()

				}

			})
		}
	}
	defer client.Close()
}

func workSetExGoRedisV7(wg *sync.WaitGroup, logger hclog.Logger, client *redis.Client, bmark bmarkSetExT) {

	defer wg.Done()
	for i := 0; i < setExInsertsPerWorker; i++ {
		set, setexDur := SetExStringGoRedisV7(client, bmark.key, bmark.value, bmark.seconds)
		if testDebugLevel > 100 {
			logger.Info(fmt.Sprintf("set:%s \t setexDur:%s", set, setexDur))
		}
	}
}
