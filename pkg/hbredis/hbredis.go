package hbredis

// redis functions for the heartbeat server

import (
	"context"
	"log"
	"runtime"
	"time"

	"errors"

	"github.com/go-redis/redis/v7"
	"github.com/mediocregopher/radix/v4"
	"github.com/mediocregopher/radix/v4/trace"
	"inet.af/netaddr"
)

func GetAddr(network *string, tcp *string, unix *string) (addr string, err error) {

	switch *network {
	case "tcp":
		addr = *tcp
	case "unix":
		addr = *unix
	default:
		//log.Fatal("network can be \"tcp\" or \"unix\" only")
		err = errors.New("network can be \"tcp\" or \"unix\" only")
		return "", err
	}
	return addr, nil
}

// ConnectRadixV4 connects and returns a RadixV4 client
// https://pkg.go.dev/github.com/mediocregopher/radix/v4
func ConnectRadixV4(ctx context.Context, network string, addr string) (client radix.Client) {

	initDoneCh := make(chan struct{})
	client, err := radix.PoolConfig{
		Size: runtime.GOMAXPROCS(0),
		Dialer: radix.Dialer{
			// https://pkg.go.dev/github.com/mediocregopher/radix/v4#Dialer.WriteFlushInterval
			// Defaults to 0, indicating Flush will be called upon each EncodeDecode
			// call without delay.
			WriteFlushInterval: 0, // No delay
		},
		Trace: trace.PoolTrace{
			InitCompleted: func(trace.PoolInitCompleted) { close(initDoneCh) },
		},
	}.New(ctx, network, addr)
	if err != nil {
		log.Fatal(err)
	}
	//defer pool.Close()

	<-initDoneCh

	return client
}

// ConnectGoRedisV7 connects and returns a go-redis v7 client
// Function includes lots of options to make it obvious what's configurable
// NOTE
// > go-redis by default opens x10 connections per CPU!!
// > using defaults except for MinIdleConns
// https://pkg.go.dev/github.com/go-redis/redis
func ConnectGoRedisV7(network string, addr string) (client *redis.Client) {

	// https://pkg.go.dev/github.com/go-redis/redis/v7@v7.4.1#Options
	client = redis.NewClient(&redis.Options{
		Network:  network,
		Addr:     addr,
		Password: "", // no password set
		DB:       0,  // use default DB
		//
		// Maximum number of socket connections.
		// Default is 10 connections per every CPU as reported by runtime.NumCPU.
		//PoolSize int
		//
		// Minimum number of idle connections which is useful when establishing
		// new connection is slow.
		MinIdleConns: runtime.GOMAXPROCS(0),
		// Default is 3 seconds.
		//ReadTimeout time.Duration
		// Connection age at which client retires (closes) the connection.
		// Default is to not close aged connections.
		//MaxConnAge time.Duration
		// Amount of time client waits for connection if all connections
		// are busy before returning an error.
		// Default is ReadTimeout + 1 second.
		//PoolTimeout time.Duration
		// Amount of time after which client closes idle connections.
		// Should be less than server's timeout.
		// Default is 5 minutes. -1 disables idle timeout check.
		//IdleTimeout time.Duration
		// Frequency of idle checks made by idle connections reaper.
		// Default is 1 minute. -1 disables idle connections reaper,
		// but idle connections are still discarded by the client
		// if IdleTimeout is set.
		//IdleCheckFrequency time.Duration
	})
	//defer client.Close()
	return client
}

// PingRadixV4 performs radix v4 PING
// https://redis.io/commands/ping
func PingRadixV4(ctx context.Context, client radix.Client) (pong string) {

	err := client.Do(ctx, radix.Cmd(&pong, "PING"))
	if err != nil {
		log.Fatal(err)
	}
	return pong
}

// PingGoRedixV7 perform go-redis v7 PING
// https://redis.io/commands/ping
// https://pkg.go.dev/github.com/go-redis/redis#Client.Ping
func PingGoRedixV7(client *redis.Client) (pong string) {
	var err error
	pong, err = client.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	return pong
}

// SetExStringRadixV4 inserts IP address strings into Redis as strings, with an expiry
func SetExStringRadixV4(ctx context.Context, client radix.Client, key string, value string, seconds string) (latency time.Duration) {

	start := time.Now()

	if err := client.Do(ctx, radix.Cmd(nil, "SETEX", key, seconds, value)); err != nil {
		log.Fatal("client.Do SET:", err)
	}
	latency = time.Since(start)

	return latency
}

// IPStringsAsByteSlice converts IP strings to []byte slices
func IPStringsAsByteSlice(key string, value string) (keyByteSlc []byte, valueByteSlc []byte) {

	keyByteSlc = IPStringToByteSlice(key)
	valueByteSlc = IPStringToByteSlice(value)

	return keyByteSlc, valueByteSlc
}

// IPStringToByteSlice convert an IP address string to []byte 32/128 bits
// See also:
// https://pkg.go.dev/inet.af/netaddr#ParseIP
// https://pkg.go.dev/inet.af/netaddr#IP.MarshalBinary
func IPStringToByteSlice(IPstr string) (ByteSlc []byte) {
	IP, err := netaddr.ParseIP(IPstr)
	if err != nil {
		log.Fatal(err)
	}

	var bErr error
	ByteSlc, bErr = IP.MarshalBinary()
	if bErr != nil {
		log.Fatal(bErr)
	}
	return ByteSlc
}

// SetExBytesRadixV4 inserts k/v as []bytes and returns the SETEX duration
// https://pkg.go.dev/github.com/mediocregopher/radix/v4#example-FlatCmd-Struct
func SetExBytesRadixV4(ctx context.Context, client radix.Client, key *[]byte, value *[]byte, seconds string) (set string, setexDur time.Duration) {

	start := time.Now()

	if err := client.Do(ctx, radix.FlatCmd(&set, "SETEX", *key, seconds, *value)); err != nil {
		log.Fatal("SetExBytesRadixV4 client.Do SET:", err)
	}

	setexDur = time.Since(start)
	return set, setexDur
}

// SetExStringGoRedisV7 inserts k/v as string
// NOTE conversion to time.Duration, which must get converted back to string for Redis
// I'm suprised go-redis v7 doesn't have a string form for the duration
func SetExStringGoRedisV7(client *redis.Client, key string, value string, seconds string) (set string, setexDur time.Duration) {

	start := time.Now()

	secs, err := time.ParseDuration(seconds + "s")
	if err != nil {
		log.Fatal(err)
	}

	set, sErr := client.Set(key, value, secs).Result()
	if sErr != nil {
		log.Fatal(sErr)
	}

	setexDur = time.Since(start)
	return set, setexDur

}
