package server

// TODO Use https://ghz.sh/ for grpc testing

import (
	"context"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"fmt"
	"log"
	"net"
	"runtime"
	"strconv"
	"sync"
	"syscall"
	"time"

	"crypto/rand"

	hclog "github.com/hashicorp/go-hclog"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"gitlab.com/dave.seddon/heartbeat/pkg/hbproto"
	"golang.org/x/sys/unix"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"google.golang.org/protobuf/types/known/wrapperspb"
	"inet.af/netaddr"
)

const (
	CountersChannelSizeCst = 100
	FakeKeySizeCst         = 10
	WorkersPerSocketCst    = 4
	MaxPacketSizeCst       = 1000
	NameSpaceCst           = "hbs"
	PulseCst               = 1 * time.Minute
	CofigFCst              = 2 * time.Hour
)

type HBST struct {
	Logger hclog.Logger
	sync.RWMutex
	WG              *sync.WaitGroup
	DoneAll         chan struct{}
	UDPPort         uint16
	ReadDeadline    time.Duration
	BytePool        sync.Pool
	SocketCount     int
	Sockets         map[int]*net.PacketConn
	ReaderCount     int
	ReadersWG       *sync.WaitGroup
	ReadersDone     map[int]map[int]chan struct{}
	PrivateKey      *rsa.PrivateKey
	PublicKey       *rsa.PublicKey
	PulseFrequency  time.Duration
	ConfigFrequency time.Duration
	DebugLevels     DebugLevelsT
}

type DebugLevelsT struct {
	New       int
	UDPReader int
	UDPStater int
}

func CheckErrorFatal(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func New(logger hclog.Logger, dA chan struct{}, wg *sync.WaitGroup, udpPort uint16, readDeadline time.Duration, maxPacketSize int) (srv *HBST) {

	debugLevels := DebugLevelsT{
		New:       0,
		UDPReader: 0,
		UDPStater: 0,
	}

	return NewFullConfig(logger, dA, wg, PulseCst, CofigFCst, udpPort, readDeadline, MaxPacketSizeCst, runtime.GOMAXPROCS(runtime.NumCPU()), WorkersPerSocketCst, debugLevels)
}

func NewFullConfig(logger hclog.Logger, dA chan struct{}, wg *sync.WaitGroup, pulseFrequency time.Duration, configFrequency time.Duration, udpPort uint16, readDeadline time.Duration, maxPacketSize int, socketCount int, readerCount int, debugLevels DebugLevelsT) (srv *HBST) {

	srv = &HBST{}

	promStats := srv.UDPPromStats()

	pool := sync.Pool{
		New: func() interface{} {
			b := make([]byte, maxPacketSize)
			return &b
		},
	}

	// nprimes defaults to 2
	// https://cs.opensource.google/go/go/+/refs/tags/go1.17:src/crypto/rsa/rsa.go;l=241
	privateKey, err := rsa.GenerateMultiPrimeKey(rand.Reader, 4, 4096)
	if err != nil {
		log.Fatal("rsa.GenerateMultiPrimeKey(rand.Reader, 4, 4096):", err)
	}

	publicKey := privateKey.PublicKey
	srv.PrivateKey = privateKey
	srv.PublicKey = &publicKey

	sockets := make(map[int]*net.PacketConn)
	readersWG := new(sync.WaitGroup)
	readersDone := make(map[int]map[int]chan struct{})

	// udpPort is uint16, because TCP/UDP port is 2^16. Not sure why golang has 'Port' as 'int'
	port := int(udpPort)
	laddr := &net.UDPAddr{
		//IP:   net.ParseIP("0.0.0.0"),
		Port: port,
	}

	for i := 0; i < socketCount; i++ {

		lc := net.ListenConfig{
			Control: func(network, address string, c syscall.RawConn) error {
				var opErr error
				err := c.Control(func(fd uintptr) {
					opErr = unix.SetsockoptInt(int(fd), unix.SOL_SOCKET, unix.SO_REUSEPORT, 1)
				})
				if err != nil {
					return err
				}
				return opErr
			},
		}

		udpSock, listenErr := lc.ListenPacket(context.Background(), "udp", laddr.String())
		if listenErr != nil {
			log.Fatal(listenErr)
		}
		sockets[i] = &udpSock

		if debugLevels.New > 10 {
			logger.Info(fmt.Sprintf("Socket opened i:%d", i))
		}

		readersDone[i] = make(map[int]chan struct{})
		for j := 0; j < readerCount; j++ {
			done := make(chan struct{}, 2)
			readersDone[i][j] = done
			readersWG.Add(1)
			go srv.UDPReader(logger, readersWG, &udpSock, readDeadline, pool, done, promStats, i, j, privateKey, debugLevels.UDPReader)
			if debugLevels.New > 10 {
				logger.Info(fmt.Sprintf("UDPReader started i:%d j:%d", i, j))
			}
		}
	}

	srv.Logger = logger
	srv.WG = wg
	srv.DoneAll = dA
	srv.UDPPort = udpPort
	srv.ReadDeadline = readDeadline
	srv.BytePool = pool
	srv.SocketCount = socketCount
	srv.Sockets = sockets
	srv.ReaderCount = readerCount
	srv.ReadersWG = readersWG
	srv.ReadersDone = readersDone
	srv.PulseFrequency = pulseFrequency
	srv.ConfigFrequency = configFrequency
	srv.DebugLevels = debugLevels

	return srv
}

// UDPReaders perform ReadFrom on a socket with a timeout
// sync.Pool []bytes being used to reduce garbage collection
// Prometheus stats collected
func (s *HBST) UDPReader(logger hclog.Logger, wg *sync.WaitGroup, udpSock *net.PacketConn, deadline time.Duration, pool sync.Pool, done <-chan struct{}, promStats PromUDPReaderStats, i int, j int, privateKey *rsa.PrivateKey, debugLevel int) {

	defer wg.Done()

	iL := strconv.FormatInt(int64(i), 10)
	jL := strconv.FormatInt(int64(j), 10)

	for keepLooping := true; keepLooping; promStats.loops.WithLabelValues(iL, jL).Inc() {

		bytes := pool.Get().(*[]byte)

		if deadline != 0 {
			// set the deadline to allow checking the done channel, otherwise readfrom blocks forever
			dlErr := (*udpSock).SetReadDeadline(time.Now().Add(deadline))
			if dlErr != nil {
				promStats.deadlineErrs.WithLabelValues(iL, jL).Inc()
				continue
			}
		}

		beforeReadFrom := time.Now()
		n, addr, rErr := (*udpSock).ReadFrom(*bytes)
		if rErr != nil {
			if rErr.(net.Error).Timeout() {
				promStats.readTimeouts.WithLabelValues(iL, jL).Inc()
			} else {
				promStats.readErrs.WithLabelValues(iL, jL).Inc()
				// logger.Info(fmt.Sprintf("%s", rErr))
			}
		} else {
			received := time.Now()
			promStats.pReadFrom.WithLabelValues(iL, jL).Observe(time.Since(beforeReadFrom).Seconds())
			promStats.bytesRead.WithLabelValues(iL, jL).Add(float64(n))

			if debugLevel > 100 {
				logger.Info(fmt.Sprintf("Packet Received: [%d] bytes from [%s] \t i:%d \t j:%d", n, addr.String(), i, j))
			}

			decryptedPB, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, privateKey, (*bytes)[0:n], nil)
			if err != nil {
				fmt.Println(err)
			}

			// https://pkg.go.dev/google.golang.org/protobuf/proto#Unmarshal
			heartbeatProto := &hbproto.HeartBeatT{}
			//unmarshalErr := proto.Unmarshal((*bytes)[0:n], heartbeatProto)
			unmarshalErr := proto.Unmarshal(decryptedPB, heartbeatProto)
			if unmarshalErr != nil {
				logger.Info(fmt.Sprintf("proto.Unmarshal(bytes,heartbeatProto) error: %s", unmarshalErr))
			}
			pool.Put(bytes)

			if debugLevel > 100 {
				s.Logger.Info(fmt.Sprintf("heartbeatProto.String():%s", heartbeatProto.String()))
			}

			// Put the address we just learnt from the socket into the protobuf
			routerIP, ok := netaddr.FromStdIP(addr.(*net.UDPAddr).IP)
			if !ok {
				log.Fatal("netaddr.FromStdIP(addr.(*net.UDPAddr).IP) error")
			}
			routerIPBytes, err := routerIP.MarshalBinary()
			if err != nil {
				log.Fatal(err)
			}
			heartbeatProto.RouterIP = wrapperspb.Bytes(routerIPBytes)

			heartbeatProto.RecievedTimestamp = timestamppb.New(received)

			if err := heartbeatProto.Timestamp.CheckValid(); err == nil {
				heartbeatProto.HalfTrip = durationpb.New(received.Sub(heartbeatProto.Timestamp.AsTime()))
			}

			// UDP calls the Pulse gRPC
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			beforeGRPC := time.Now()
			_, perr := s.Pulse(ctx, heartbeatProto)
			if perr != nil {
				log.Fatal(perr)
			}
			promStats.pGRPCDuration.WithLabelValues(iL, jL).Observe(time.Since(beforeGRPC).Seconds())

		}

		// non-blocking check for done
		select {
		case <-done:
			keepLooping = false
		default:
		}
	}
}

// Pulse is the gRPC API which accepts the hbproto and inserts it into the data store (Redis)
func (s *HBST) Pulse(ctx context.Context, hb *hbproto.HeartBeatT) (*hbproto.EmptyT, error) {

	s.Logger.Info("Pulse - INSERT INTO REDIS HERE")

	// unmarshall IPs here
	var lanIP netaddr.IP
	lerr := lanIP.UnmarshalBinary(hb.LANIP.GetValue())
	if lerr != nil {
		log.Fatal(lerr)
	}
	var routerIP netaddr.IP
	rerr := routerIP.UnmarshalBinary(hb.RouterIP.GetValue())
	if rerr != nil {
		log.Fatal(rerr)
	}

	s.Logger.Info(fmt.Sprintf("hb.String():%s \t lanIP.String():%s \t routerIP.String():%s", hb.String(), lanIP.String(), routerIP.String()))

	return &hbproto.EmptyT{}, nil
}

// GetPublicKey will return a public key
func (s *HBST) GetPublicKey(ctx context.Context, e *hbproto.EmptyT) (*hbproto.PublicKeyT, error) {

	return &hbproto.PublicKeyT{PublicKey: x509.MarshalPKCS1PublicKey(s.PublicKey)}, nil
}

// GetPulseFrequency tells the client what frequency to pulse at
func (s *HBST) GetPulseFrequency(ctx context.Context, e *hbproto.EmptyT) (*hbproto.PulseFrequencyT, error) {

	return &hbproto.PulseFrequencyT{Duration: durationpb.New(s.PulseFrequency)}, nil
}

// GetConfigFrequency tells the client what frequency check for new config at
func (s *HBST) GetConfigFrequency(ctx context.Context, e *hbproto.EmptyT) (*hbproto.ConfigFrequencyT, error) {

	return &hbproto.ConfigFrequencyT{Duration: durationpb.New(s.ConfigFrequency)}, nil
}

// GetClientConfig tells the client the full set of configuration (pulse, config frequencies, and public key)
func (s *HBST) GetClientConfig(ctx context.Context, e *hbproto.EmptyT) (*hbproto.ClientConfigT, error) {

	pf, err := s.GetPulseFrequency(ctx, e)
	if err != nil {
		log.Fatal("s.GetPulseFrequency(ctx, e):", err)
	}

	cf, err := s.GetConfigFrequency(ctx, e)
	if err != nil {
		log.Fatal("s.GetConfigFrequency(ctx, e):", err)
	}

	pk, err := s.GetPublicKey(ctx, e)
	if err != nil {
		log.Fatal("s.GetPublicKey(ctx, e):", err)
	}

	return &hbproto.ClientConfigT{
		PulseFrequency:  pf,
		ConfigFrequency: cf,
		PublicKey:       pk,
	}, nil
}

type PromUDPReaderStats struct {
	loops         *prometheus.CounterVec
	bytesRead     *prometheus.CounterVec
	readErrs      *prometheus.CounterVec
	readTimeouts  *prometheus.CounterVec
	deadlineErrs  *prometheus.CounterVec
	pReadFrom     *prometheus.SummaryVec
	pGRPCDuration *prometheus.SummaryVec
}

// UDPPromStats initializes Prometheous counters
func (s *HBST) UDPPromStats() (promStats PromUDPReaderStats) {

	pLoops := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: NameSpaceCst,
			Subsystem: "UDPReader",
			Name:      "loops",
			Help:      "UDPReader loops counter",
		},
		[]string{"i", "j"},
	)

	pBytesRead := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: NameSpaceCst,
			Subsystem: "UDPReader",
			Name:      "bytesRead",
			Help:      "UDPReader loop bytes counter",
		},
		[]string{"i", "j"},
	)

	pReadErrs := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: NameSpaceCst,
			Subsystem: "UDPReader",
			Name:      "readErrs",
			Help:      "UDPReader readErrs counter",
		},
		[]string{"i", "j"},
	)

	pReadTimeouts := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: NameSpaceCst,
			Subsystem: "UDPReader",
			Name:      "readTimeouts",
			Help:      "UDPReader readTimeouts counter",
		},
		[]string{"i", "j"},
	)

	pDlErrs := promauto.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: NameSpaceCst,
			Subsystem: "UDPReader",
			Name:      "deadlineErrs",
			Help:      "UDPReader deadline errors (dlErrs)  counter",
		},
		[]string{"i", "j"},
	)

	// Warning - Summaries are relatiely expensive
	// See also: https://prometheus.io/docs/practices/histograms/
	// https://godoc.org/github.com/prometheus/client_golang/prometheus#SummaryOpts
	pReadFrom := promauto.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace: "UDPReader",
			Subsystem: "ReadFrom",
			Name:      "Duration",
			Help:      "UDPReader ReadFrom to packet received duration",
			Objectives: map[float64]float64{ // 50th, 99th
				0.5:  0.05,
				0.99: 0.001},
			MaxAge: 1 * time.Minute, // 1 minutes of data
		},
		[]string{"i", "j"},
	)

	pGRPCDuration := promauto.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace: "UDPReader",
			Subsystem: "insert",
			Name:      "grpcDuration",
			Help:      "UDPReader insert gRPC Duration",
			Objectives: map[float64]float64{ // 50th, 99th
				0.5:  0.05,
				0.99: 0.001},
			MaxAge: 1 * time.Minute, // 1 minutes of data
		},
		[]string{"i", "j"},
	)

	promStats = PromUDPReaderStats{
		pLoops,
		pBytesRead,
		pReadErrs,
		pReadTimeouts,
		pDlErrs,
		pReadFrom,
		pGRPCDuration,
	}

	return promStats
}
