// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package hbproto

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// HeartBeatClient is the client API for HeartBeat service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HeartBeatClient interface {
	Pulse(ctx context.Context, in *HeartBeatT, opts ...grpc.CallOption) (*EmptyT, error)
	GetPublicKey(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*PublicKeyT, error)
	GetPulseFrequency(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*PulseFrequencyT, error)
	GetConfigFrequency(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*ConfigFrequencyT, error)
	GetClientConfig(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*ClientConfigT, error)
}

type heartBeatClient struct {
	cc grpc.ClientConnInterface
}

func NewHeartBeatClient(cc grpc.ClientConnInterface) HeartBeatClient {
	return &heartBeatClient{cc}
}

func (c *heartBeatClient) Pulse(ctx context.Context, in *HeartBeatT, opts ...grpc.CallOption) (*EmptyT, error) {
	out := new(EmptyT)
	err := c.cc.Invoke(ctx, "/hbproto.HeartBeat/Pulse", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *heartBeatClient) GetPublicKey(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*PublicKeyT, error) {
	out := new(PublicKeyT)
	err := c.cc.Invoke(ctx, "/hbproto.HeartBeat/GetPublicKey", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *heartBeatClient) GetPulseFrequency(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*PulseFrequencyT, error) {
	out := new(PulseFrequencyT)
	err := c.cc.Invoke(ctx, "/hbproto.HeartBeat/GetPulseFrequency", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *heartBeatClient) GetConfigFrequency(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*ConfigFrequencyT, error) {
	out := new(ConfigFrequencyT)
	err := c.cc.Invoke(ctx, "/hbproto.HeartBeat/GetConfigFrequency", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *heartBeatClient) GetClientConfig(ctx context.Context, in *EmptyT, opts ...grpc.CallOption) (*ClientConfigT, error) {
	out := new(ClientConfigT)
	err := c.cc.Invoke(ctx, "/hbproto.HeartBeat/GetClientConfig", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HeartBeatServer is the server API for HeartBeat service.
// All implementations should embed UnimplementedHeartBeatServer
// for forward compatibility
type HeartBeatServer interface {
	Pulse(context.Context, *HeartBeatT) (*EmptyT, error)
	GetPublicKey(context.Context, *EmptyT) (*PublicKeyT, error)
	GetPulseFrequency(context.Context, *EmptyT) (*PulseFrequencyT, error)
	GetConfigFrequency(context.Context, *EmptyT) (*ConfigFrequencyT, error)
	GetClientConfig(context.Context, *EmptyT) (*ClientConfigT, error)
}

// UnimplementedHeartBeatServer should be embedded to have forward compatible implementations.
type UnimplementedHeartBeatServer struct {
}

func (UnimplementedHeartBeatServer) Pulse(context.Context, *HeartBeatT) (*EmptyT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Pulse not implemented")
}
func (UnimplementedHeartBeatServer) GetPublicKey(context.Context, *EmptyT) (*PublicKeyT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPublicKey not implemented")
}
func (UnimplementedHeartBeatServer) GetPulseFrequency(context.Context, *EmptyT) (*PulseFrequencyT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPulseFrequency not implemented")
}
func (UnimplementedHeartBeatServer) GetConfigFrequency(context.Context, *EmptyT) (*ConfigFrequencyT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetConfigFrequency not implemented")
}
func (UnimplementedHeartBeatServer) GetClientConfig(context.Context, *EmptyT) (*ClientConfigT, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetClientConfig not implemented")
}

// UnsafeHeartBeatServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HeartBeatServer will
// result in compilation errors.
type UnsafeHeartBeatServer interface {
	mustEmbedUnimplementedHeartBeatServer()
}

func RegisterHeartBeatServer(s grpc.ServiceRegistrar, srv HeartBeatServer) {
	s.RegisterService(&HeartBeat_ServiceDesc, srv)
}

func _HeartBeat_Pulse_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HeartBeatT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeartBeatServer).Pulse(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hbproto.HeartBeat/Pulse",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeartBeatServer).Pulse(ctx, req.(*HeartBeatT))
	}
	return interceptor(ctx, in, info, handler)
}

func _HeartBeat_GetPublicKey_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeartBeatServer).GetPublicKey(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hbproto.HeartBeat/GetPublicKey",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeartBeatServer).GetPublicKey(ctx, req.(*EmptyT))
	}
	return interceptor(ctx, in, info, handler)
}

func _HeartBeat_GetPulseFrequency_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeartBeatServer).GetPulseFrequency(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hbproto.HeartBeat/GetPulseFrequency",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeartBeatServer).GetPulseFrequency(ctx, req.(*EmptyT))
	}
	return interceptor(ctx, in, info, handler)
}

func _HeartBeat_GetConfigFrequency_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeartBeatServer).GetConfigFrequency(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hbproto.HeartBeat/GetConfigFrequency",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeartBeatServer).GetConfigFrequency(ctx, req.(*EmptyT))
	}
	return interceptor(ctx, in, info, handler)
}

func _HeartBeat_GetClientConfig_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EmptyT)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HeartBeatServer).GetClientConfig(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hbproto.HeartBeat/GetClientConfig",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HeartBeatServer).GetClientConfig(ctx, req.(*EmptyT))
	}
	return interceptor(ctx, in, info, handler)
}

// HeartBeat_ServiceDesc is the grpc.ServiceDesc for HeartBeat service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var HeartBeat_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "hbproto.HeartBeat",
	HandlerType: (*HeartBeatServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Pulse",
			Handler:    _HeartBeat_Pulse_Handler,
		},
		{
			MethodName: "GetPublicKey",
			Handler:    _HeartBeat_GetPublicKey_Handler,
		},
		{
			MethodName: "GetPulseFrequency",
			Handler:    _HeartBeat_GetPulseFrequency_Handler,
		},
		{
			MethodName: "GetConfigFrequency",
			Handler:    _HeartBeat_GetConfigFrequency_Handler,
		},
		{
			MethodName: "GetClientConfig",
			Handler:    _HeartBeat_GetClientConfig_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "hbproto.proto",
}
